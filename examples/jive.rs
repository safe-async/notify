use std::time::Duration;

fn main() {
    jive::block_on(async move {
        let notify = notify::Notify::new();

        println!("Queuing one notify");
        notify.notify_one();

        let mut listener = notify.listener();

        listener.listen().await;
        println!("immediate");

        let mut handles = Vec::new();
        for i in 0..4 {
            let mut listener = notify.listener();

            handles.push(jive::spawn(async move {
                for _ in 0..5 {
                    listener.listen().await;
                    println!("woken {}!", i);
                }
            }));
        }

        jive::spawn(async move {
            loop {
                jive::time::sleep(Duration::from_millis(200)).await;
                notify.notify_one();
            }
        });

        for handle in handles {
            let _ = handle.await;
        }
    })
}
