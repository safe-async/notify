use std::time::Duration;

fn main() {
    let notify = notify::Notify::new();

    println!("Queuing one notify");
    notify.notify_one();

    let listener = notify.listener();

    listener.listen_blocking();
    println!("immediate");

    let mut handles = Vec::new();
    for i in 0..4 {
        let listener = notify.listener();

        handles.push(std::thread::spawn(move || {
            for _ in 0..5 {
                listener.listen_blocking();
                println!("woken {}!", i);
            }
        }));
    }

    std::thread::spawn(move || loop {
        std::thread::sleep(Duration::from_millis(200));
        notify.notify_one();
    });

    for handle in handles {
        let _ = handle.join();
    }
}
